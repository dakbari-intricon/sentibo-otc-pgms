run ('../sentibo_colors.m');

#x tick labels
freqs_x = [250 500 1000 2000 4000 8000 16000];
#y tick labels
amps_y = [-10 0 10 20 30 40 50 60 70 80 90 100 110 120 130 140];

#Gain y tick labels
amps_yy = [-25 -20 -15 -10 -5 0 +5 +10 +15 +20 +25];

#global variables
freqs_aud = [250.0 500.0 750.0 1000.0 1500.0 2000.0 3000.0 4000.0 6000.0 8000.0 10000.0 12500.0];
freqs_aud_red = [250.0 500.0 750.0 1000.0 1500.0 2000.0 3000.0 4000.0 6000.0 8000.0];
freqs_thrd = [200.0 250.0 315.0 400.0 500.0 630.0 800.0 1000.0 1250.0 1600.0 2000.0 2500.0 3150.0 4000.0 5000.0 6300.0 8000.0 10000.0 12500.0 16000.0];
freqs_twth = [200.0 210.0 225.0 235.0 250.0 265.0 280.0 300.0 315.0 335.0 355.0 375.0 400.0 420.0 450.0 470.0 500.0 530.0 560.0 595.0 630.0 670.0 710.0 750.0 800.0 840.0 900.0 945.0 1000.0 1060.0 1120.0 1190.0 1250.0 1335.0 1400.0 1500.0 1600.0 1680.0 1800.0 1890.0 2000.0 2120.0 2240.0 2380.0 2500.0 2670.0 2800.0 3000.0 3150.0 3365.0 3550.0 3775.0 4000.0 4240.0 4500.0 4760.0 5000.0 5340.0 5600.0 6000.0 6300.0 6725.0 7100.0 7550.0 8000.0 8500.0 9000.0 9500.0 10000.0 10600.0 11200.0 11800.0 12500.0];
map_spl_thrd = [21.8 18.8 16.0 13.7 12.0 10.8 9.8 9.1 9.3 11.8 15.3 16.6 15.0 13.5 13.7 15.6 18.3];
map_spl_aud = [17.5 10.2 8.5 9.3 9.5 12.9 13.0 15.0 16.0 18.3 22.1 26.9];

### Left ear (NAL-NL2) values
soft_twth_l = [39.7 40.1 40.4 41.5 42.3 42.3 42.0 41.7 42.2 43.0 46.2 47.2 47.8 47.7 47.4 46.4 46.8 48.1 48.6 48.9 48.5 47.7 45.9 44.6 43.6 43.5 43.5 44.2 45.1 47.6 49.3 51.6 53.2 54.2 54.2 55.4 56.2 57.7 58.4 58.9 58.3 57.2 55.2 53.5 53.5 54.0 54.6 55.0 55.8 58.8 62.6 64.6 66.0 66.3 65.9 64.4 62.4 59.0 55.8 52.9 46.1 40.7 39.2 37.9 36.3 33.5 33.8 34.4 35.0 35.6 35.9 36.1 36.1];
mid_twth_l = [52.5 52.9 53.3 54.2 54.9 55.1 55.1 55.4 56.5 57.4 59.2 60.0 60.3 60.2 60.1 59.6 60.3 61.1 61.4 61.5 61.1 60.4 59.4 59.1 58.8 59.0 59.2 59.8 60.5 62.8 64.2 66.1 67.4 68.1 67.9 68.7 69.6 70.8 71.8 72.3 71.9 71.0 69.5 68.1 68.6 69.2 69.7 70.2 71.3 74.3 77.4 79.2 80.3 80.4 79.7 78.1 75.8 72.4 69.5 66.5 59.7 54.3 53.1 51.8 49.7 40.5 34.0 34.6 35.1 35.7 36.0 36.1 36.1];
loud_twth_l = [53.5 54.8 56.1 58.7 60.4 61.9 63.0 64.0 65.5 66.3 67.4 67.8 68.0 68.1 68.4 68.8 70.1 71.5 72.2 72.8 72.8 72.7 72.1 72.0 71.9 72.5 73.1 74.2 75.5 78.1 79.9 81.9 83.1 83.7 83.4 83.7 84.4 85.7 86.6 87.1 86.7 85.6 83.8 82.2 82.3 82.8 83.3 83.9 84.9 87.3 89.8 91.2 91.9 91.7 90.6 88.4 85.5 81.4 77.4 74.2 66.5 60.6 59.3 58.0 55.7 46.8 36.9 36.8 36.5 36.1 36.4 36.6 36.6];
mpo_twth_l = [78.8 79.3 79.8 80.4 80.9 81.7 82.4 83.2 84.0 84.7 85.4 86.1 86.8 87.0 87.2 87.3 87.5 88.4 89.2 90.1 91.0 90.9 90.9 90.8 90.8 91.6 92.3 93.1 93.9 95.0 96.1 97.3 98.4 95.7 93.0 90.3 87.5 88.0 88.5 89.0 89.4 88.7 88.0 87.3 86.6 87.4 88.3 89.2 90.1 92.2 94.4 96.5 98.7 96.6 94.5 92.5 90.4 85.7 80.9 76.2 71.5 68.9 66.2 63.6 61.0 60.8 60.7 60.6 60.4 61.8 63.2 64.5 65.9];
soft_target_l = [40.9 42.5 43.8 45.8 53.5 61.9 67.3 69.4 61.1 57.0];
mid_target_l = [55.9 57.5 55.3 56.7 62.5 69.1 74.1 76.9 69.4 65.9];
loud_target_l = [70.9 72.5 68.0 65.6 69.8 74.8 79.0 81.9 75.2 72.5];
spl_thrs_l = [32.5 25.2 ((34.3+25.2)/2) 34.3 ((34.3+52.9)/2) 52.9 63.0 70.0 86.0 88.3];
spl_ucl_l = [91 98 103 107 110 113 117 120 121 120];
###

### Right ear (NAL-NL2) values
soft_twth_r = [40.7 41.1 41.5 42.8 43.6 43.6 43.3 42.9 43.2 43.9 47.0 48.1 48.7 48.8 48.7 48.0 48.4 49.6 50.0 50.3 49.9 49.2 47.6 46.9 46.6 47.0 47.3 48.1 48.9 51.5 53.3 56.1 58.2 59.4 59.6 60.5 61.2 62.9 64.1 64.7 64.4 63.6 61.7 59.7 59.9 60.5 60.9 60.8 60.7 61.4 63.6 65.3 67.0 67.6 67.3 66.1 64.3 60.0 54.2 50.9 42.6 37.3 36.5 35.8 34.9 33.3 33.8 34.5 35.1 35.7 36.0 36.1 36.1];
mid_twth_r = [53.3 53.8 54.2 55.2 55.9 56.1 56.0 56.3 57.4 58.2 60.0 60.7 61.1 61.1 61.2 61.0 61.6 62.5 62.7 62.8 62.5 62.0 61.2 61.3 61.3 61.9 62.4 63.0 63.7 66.0 67.7 70.1 71.8 72.9 73.0 73.8 74.6 76.4 77.6 78.2 78.0 77.1 75.3 73.5 73.8 74.5 75.0 75.0 75.1 76.1 78.0 79.5 80.8 81.1 80.5 79.1 77.0 72.7 67.3 64.0 55.7 50.9 50.1 48.9 46.7 38.0 34.0 34.6 35.2 35.7 36.0 36.2 36.2];
loud_twth_r = [54.4 55.7 57.0 59.5 61.3 62.8 63.7 64.8 66.3 67.1 68.2 68.6 68.8 69.0 69.5 70.1 71.4 72.8 73.4 74.0 74.1 74.0 73.7 74.0 74.4 75.2 76.2 77.3 78.6 81.3 83.4 85.9 87.5 88.4 88.4 88.7 89.2 91.0 92.1 92.7 92.4 91.6 89.6 87.8 87.6 87.8 88.0 88.0 88.2 89.1 90.3 91.3 91.9 91.7 90.5 88.6 86.0 81.2 75.5 72.0 63.0 57.1 56.1 54.9 52.6 43.9 37.4 37.3 37.0 36.4 36.6 36.8 36.9];
mpo_twth_r = [79.9 80.5 81.1 81.8 82.4 83.0 83.5 84.0 84.6 85.3 86.1 86.9 87.6 88.0 88.3 88.6 89.0 89.8 90.7 91.5 92.3 92.5 92.8 93.0 93.2 94.2 95.1 96.1 97.1 98.3 99.5 100.6 101.8 99.0 96.1 93.3 90.4 91.6 92.8 94.0 95.1 94.5 93.8 93.2 92.5 92.8 93.0 93.3 93.5 94.5 95.4 96.3 97.3 94.4 91.6 88.8 88.4 83.4 78.5 73.6 68.6 67.0 65.5 63.9 62.3 62.2 62.1 62.0 61.8 63.1 64.4 65.7 66.9];
soft_target_r = [40.9 42.5 44.7 48.4 55.7 63.8 67.8 67.7 58.8 54.2];
mid_target_r = [55.9 57.5 56.9 59.4 65.0 71.4 75.4 76.1 68.4 64.9];
loud_target_r = [70.9 72.5 68.7 67.9 72.3 77.4 80.8 81.8 75.4 72.9];
spl_thrs_r = [27.5 20.2 28.5 39.3 ((52.9+39.3)/2) 52.9 68.0 75.0 81.0 78.3];
spl_ucl_r = [91 98 103 107 110 113 117 120 121 120];
###

### Left ear (Sentibo) values
soft_twth_sl = [48.2 48.6 49.0 49.9 50.5 50.5 50.4 50.6 51.8 52.9 54.9 55.7 56.1 55.9 55.8 55.2 55.7 56.6 57.0 57.3 57.0 56.6 55.8 55.9 56.2 56.9 57.4 58.2 59.3 62.4 64.8 69.0 71.7 73.6 74.1 74.6 74.0 73.0 71.3 70.2 67.3 65.7 63.9 62.1 60.3 58.7 56.7 54.3 51.8 57.2 62.8 65.0 66.5 66.9 66.7 65.4 64.0 61.8 59.4 56.9 50.7 46.4 46.1 45.1 43.4 36.7 34.0 34.5 35.1 35.6 36.0 36.1 36.2];
mid_twth_sl = [59.2 59.6 59.9 60.3 60.4 60.5 60.6 61.4 62.9 64.1 65.6 66.2 66.5 66.4 66.4 66.0 66.7 67.1 67.2 67.4 67.6 68.3 69.1 70.7 71.9 73.3 74.2 75.0 75.9 78.6 80.6 84.2 86.9 88.6 89.0 89.4 88.7 87.5 85.8 84.7 82.1 80.5 79.1 77.5 76.1 74.6 72.7 70.3 67.5 73.4 78.7 80.7 82.0 82.3 82.0 80.6 79.2 77.1 74.7 72.2 65.9 62.2 62.2 61.2 59.5 50.5 36.1 35.7 35.4 35.8 36.0 36.2 36.2];
loud_twth_sl = [47.7 48.2 49.1 50.9 53.1 55.1 56.3 57.2 58.0 58.4 59.0 59.2 59.3 59.8 60.3 60.9 62.0 62.8 63.4 64.9 66.6 68.9 71.8 74.8 77.3 79.5 81.5 82.8 84.4 88.0 91.5 96.1 99.2 100.7 101.3 101.5 101.0 100.1 99.5 98.9 97.6 96.5 95.2 94.2 93.3 92.1 90.0 87.6 85.1 89.5 94.2 96.0 97.0 97.0 96.0 93.6 91.2 88.5 85.8 83.0 75.6 71.1 71.1 70.2 68.5 59.8 46.1 45.2 43.3 40.2 39.3 39.2 38.9];
mpo_twth_sl = [63.5 63.0 62.5 62.0 61.5 61.4 61.4 61.4 61.4 62.1 62.8 63.4 64.1 64.3 64.4 64.5 64.7 67.4 70.1 72.8 75.5 79.3 83.1 86.9 90.7 92.8 94.8 96.8 98.9 100.9 103.0 105.1 107.1 107.1 107.1 107.1 107.2 104.9 102.7 100.4 98.2 98.7 99.3 99.9 100.4 98.2 96.0 93.8 91.5 95.8 100.0 104.3 108.5 106.8 105.0 103.2 101.4 97.2 93.0 88.7 84.5 82.6 80.7 78.8 76.8 73.7 70.6 67.5 64.4 64.3 64.3 64.3 64.3];
# same as NL2 -v
#{
soft_target_l = [40.9 42.5 43.8 45.8 53.5 61.9 67.3 69.4 61.1 57.0];
mid_target_l = [55.9 57.5 55.3 56.7 62.5 69.1 74.1 76.9 69.4 65.9];
loud_target_l = [70.9 72.5 68.0 65.6 69.8 74.8 79.0 81.9 75.2 72.5];
spl_thrs_l = [32.5 25.2 ((34.3+25.2)/2) 34.3 ((34.3+52.9)/2) 52.9 63.0 70.0 86.0 88.3];
spl_ucl_l = [91 98 103 107 110 113 117 120 121 120];
#}
###

### Right ear (Sentibo) values
soft_twth_sr = [48.0 48.4 48.7 49.7 50.2 50.3 50.1 50.4 51.5 52.5 54.5 55.2 55.6 55.6 55.6 55.3 56.0 57.0 57.5 57.8 57.6 57.4 56.7 57.0 57.4 58.2 58.7 59.5 60.4 63.2 65.6 69.5 72.1 73.8 74.3 74.5 73.7 72.6 71.1 70.3 68.5 67.0 65.0 62.7 60.7 58.7 56.7 53.9 51.1 56.7 62.2 64.4 65.9 66.4 66.2 65.1 63.8 61.9 59.4 56.8 50.3 47.8 47.8 46.9 45.1 37.1 33.9 34.5 35.2 35.7 36.0 36.1 36.2];
mid_twth_sr = [58.8 59.2 59.4 59.7 59.8 59.9 60.0 60.8 62.3 63.5 65.0 65.5 65.7 65.7 65.8 65.6 66.4 67.0 67.2 67.5 67.7 68.4 69.4 70.9 72.2 73.6 74.5 75.2 76.0 78.7 80.8 84.6 87.2 88.9 89.3 89.6 88.8 87.6 86.0 85.2 83.3 82.0 80.3 78.4 76.7 74.9 72.7 69.8 66.8 71.9 77.2 79.4 80.7 81.2 80.9 79.8 78.4 76.6 74.2 71.6 65.4 63.3 63.6 62.8 61.0 51.2 36.2 35.8 35.4 35.7 36.0 36.2 36.2];
loud_twth_sr = [46.9 47.4 48.3 50.1 52.4 54.2 55.3 56.2 56.9 57.3 57.7 58.0 58.0 58.4 59.0 59.7 61.0 61.8 62.5 64.0 65.8 68.3 71.5 74.5 77.0 79.2 81.2 82.5 84.1 87.7 91.5 96.4 99.7 101.2 101.8 101.9 101.3 100.4 99.8 99.4 98.3 97.2 95.5 93.9 92.5 91.0 89.1 86.3 84.0 88.8 93.5 95.3 96.1 96.0 95.1 92.7 90.5 88.4 86.2 83.2 75.2 72.2 72.3 71.5 69.7 60.3 45.6 44.5 42.7 40.5 39.9 40.0 39.5];
mpo_twth_sr = [67.2 66.4 65.7 64.9 64.2 63.9 63.6 63.4 63.1 63.2 63.3 63.5 63.6 63.6 63.6 63.6 63.6 66.4 69.1 71.9 74.7 78.6 82.5 86.4 90.3 92.4 94.5 96.6 98.6 100.3 101.9 103.5 105.2 103.6 102.1 100.6 100.2 99.8 99.4 99.0 98.6 99.2 99.9 100.5 101.2 98.6 96.0 93.4 90.7 95.1 99.5 103.9 108.3 107.0 105.7 104.4 103.1 97.7 92.4 87.1 81.8 80.4 79.0 77.6 76.2 74.1 72.0 69.9 67.8 67.5 67.3 67.1 66.9];
# same as NL2 -v
#{
soft_target_r = [40.9 42.5 44.7 48.4 55.7 63.8 67.8 67.7 58.8 54.2];
mid_target_r = [55.9 57.5 56.9 59.4 65.0 71.4 75.4 76.1 68.4 64.9];
loud_target_r = [70.9 72.5 68.7 67.9 72.3 77.4 80.8 81.8 75.4 72.9];
spl_thrs_r = [27.5 20.2 28.5 39.3 ((52.9+39.3)/2) 52.9 68.0 75.0 81.0 78.3];
spl_ucl_r = [91 98 103 107 110 113 117 120 121 120];
#}
###

# plot left ear nal-nl2
#{
figure;
clf;
hold on;
semilogx ( freqs_twth, soft_twth_l, 'color', [0 0.808 0] );
semilogx ( freqs_twth, mid_twth_l, 'color', [0.937 0 0.937] );
semilogx ( freqs_twth, loud_twth_l, 'color', [0 0.808 0.808] );
semilogx ( freqs_twth, mpo_twth_l, 'color', [1 0.651 0], ".-" );
semilogx ( freqs_aud_red, soft_target_l, 'color', [0 0.808 0], "+" );
semilogx ( freqs_aud_red, mid_target_l, 'color', [0.937 0 0.937], "+" );
semilogx ( freqs_aud_red, loud_target_l, 'color', [0 0.808 0.808], "+" );
semilogx ( freqs_aud_red, spl_thrs_l, 'color', [0 0 0.88], "x-" );
semilogx ( freqs_aud_red, spl_ucl_l, 'color', [0 0 0], '*' );
semilogx ( freqs_aud, map_spl_aud, 'color', [0 0 0], "--" );
ylim ([-10 140]);
xlim ([200 16000]);
xticks(freqs_x);
xticklabels(freqs_x);
yticks(amps_y);
yticklabels(amps_y);
title("SP1 NAL-NL2 Left Ear", "fontsize", 18, "fontweight", "bold");
ylabel("dB SPL in HA-4 Coupler", "fontsize", 12, "fontweight", "bold");
xlabel("Frequency in Hz", "fontsize", 12, "fontweight", "bold");
#}
###

# plot right ear nal-nl2
#{
figure;
clf;
hold on;
semilogx ( freqs_twth, soft_twth_r, 'color', [0 0.808 0] );
semilogx ( freqs_twth, mid_twth_r, 'color', [0.937 0 0.937] );
semilogx ( freqs_twth, loud_twth_r, 'color', [0 0.808 0.808] );
semilogx ( freqs_twth, mpo_twth_r, 'color', [1 0.651 0], ".-" );
semilogx ( freqs_aud_red, soft_target_r, 'color', [0 0.808 0], "+" );
semilogx ( freqs_aud_red, mid_target_r, 'color', [0.937 0 0.937], "+" );
semilogx ( freqs_aud_red, loud_target_r, 'color', [0 0.808 0.808], "+" );
semilogx ( freqs_aud_red, spl_thrs_r, 'color', [0.88 0 0], "o-" );
semilogx ( freqs_aud_red, spl_ucl_r, 'color', [0 0 0], '*' );
semilogx ( freqs_aud, map_spl_aud, 'color', [0 0 0], "--" );
ylim ([-10 140]);
xlim ([200 16000]);
xticks(freqs_x);
xticklabels(freqs_x);
yticks(amps_y);
yticklabels(amps_y);
title("SP1 NAL-NL2 Right Ear", "fontsize", 18, "fontweight", "bold");
ylabel("dB SPL in HA-4 Coupler", "fontsize", 12, "fontweight", "bold");
xlabel("Frequency in Hz", "fontsize", 12, "fontweight", "bold");
#}
###

# plot left ear nal-nl2
#{
figure;
clf;
hold on;
semilogx ( freqs_twth, soft_twth_sl, 'color', [0 0.808 0] );
semilogx ( freqs_twth, mid_twth_sl, 'color', [0.937 0 0.937] );
semilogx ( freqs_twth, loud_twth_sl, 'color', [0 0.808 0.808] );
semilogx ( freqs_twth, mpo_twth_sl, 'color', [1 0.651 0], ".-" );
semilogx ( freqs_aud_red, soft_target_l, 'color', [0 0.808 0], "+" );
semilogx ( freqs_aud_red, mid_target_l, 'color', [0.937 0 0.937], "+" );
semilogx ( freqs_aud_red, loud_target_l, 'color', [0 0.808 0.808], "+" );
semilogx ( freqs_aud_red, spl_thrs_l, 'color', [0 0 0.88], "x-" );
semilogx ( freqs_aud_red, spl_ucl_l, 'color', [0 0 0], '*' );
semilogx ( freqs_aud, map_spl_aud, 'color', [0 0 0], "--" );
ylim ([-10 140]);
xlim ([200 16000]);
xticks(freqs_x);
xticklabels(freqs_x);
yticks(amps_y);
yticklabels(amps_y);
title("SP1 Sentibo G10C Left Ear", "fontsize", 18, "fontweight", "bold");
ylabel("dB SPL in HA-4 Coupler", "fontsize", 12, "fontweight", "bold");
xlabel("Frequency in Hz", "fontsize", 12, "fontweight", "bold");
#}
### 

# plot right ear nal-nl2
#{
figure;
clf;
hold on;
semilogx ( freqs_twth, soft_twth_sr, 'color', [0 0.808 0] );
semilogx ( freqs_twth, mid_twth_sr, 'color', [0.937 0 0.937] );
semilogx ( freqs_twth, loud_twth_sr, 'color', [0 0.808 0.808] );
semilogx ( freqs_twth, mpo_twth_sr, 'color', [1 0.651 0], ".-" );
semilogx ( freqs_aud_red, soft_target_r, 'color', [0 0.808 0], "+" );
semilogx ( freqs_aud_red, mid_target_r, 'color', [0.937 0 0.937], "+" );
semilogx ( freqs_aud_red, loud_target_r, 'color', [0 0.808 0.808], "+" );
semilogx ( freqs_aud_red, spl_thrs_r, 'color', [0.88 0 0], "o-" );
semilogx ( freqs_aud_red, spl_ucl_r, 'color', [0 0 0], '*' );
semilogx ( freqs_aud, map_spl_aud, 'color', [0 0 0], "--" );
ylim ([-10 140]);
xlim ([200 16000]);
xticks(freqs_x);
xticklabels(freqs_x);
yticks(amps_y);
yticklabels(amps_y);
title("SP1 Sentibo G10C Right Ear", "fontsize", 18, "fontweight", "bold");
ylabel("dB SPL in HA-4 Coupler", "fontsize", 12, "fontweight", "bold");
xlabel("Frequency in Hz", "fontsize", 12, "fontweight", "bold");
#}
###

# experimental block Sentibo vs NL2 Left
#{
figure;
clf;
hold on;
#zero line
semilogx ( freqs_twth, -5, 'color', [0 0 0], "." );
semilogx ( freqs_twth, 0, 'color', [0 0 0], "." );
semilogx ( freqs_twth, 5, 'color', [0 0 0], "." );
# nl2 minus sentibo (soft)
semilogx ( freqs_twth, (soft_twth_l-soft_twth_sl) );
# sentibo minus nl2 (soft)
semilogx ( freqs_twth, (soft_twth_sl-soft_twth_l), 'color', [0 0.808 0], '-;Soft;' );
semilogx ( freqs_twth, (mid_twth_sl-mid_twth_l), 'color', [0.937 0 0.937], '-;Mid;' );
semilogx ( freqs_twth, (loud_twth_sl-loud_twth_l), 'color', [0 0.808 0.808], '-;Loud;' );
ylim ([-25 25]);
xlim ([200 16000]);
xticks(freqs_x);
xticklabels(freqs_x);
yticks(amps_yy);
yticklabels(amps_yy);
title("SP1 Sentibo G10C Left Ear\n Sentibo minus NL2", "fontsize", 18, "fontweight", "bold");
ylabel("dB Gain in HA-4 Coupler", "fontsize", 12, "fontweight", "bold");
xlabel("Frequency in Hz", "fontsize", 12, "fontweight", "bold");
legend("location", "northeast");
#}

# experimental block Sentibo vs NL2 Right
#{
figure;
clf;
hold on;
#zero line
semilogx ( freqs_twth, -5, 'color', [0 0 0], "." );
semilogx ( freqs_twth, 0, 'color', [0 0 0], "." );
semilogx ( freqs_twth, 5, 'color', [0 0 0], "." );
# nl2 minus sentibo (soft)
semilogx ( freqs_twth, (soft_twth_l-soft_twth_sl) );
# sentibo minus nl2 (soft)
semilogx ( freqs_twth, (soft_twth_sr-soft_twth_r), 'color', [0 0.808 0], '-;Soft;' );
semilogx ( freqs_twth, (mid_twth_sr-mid_twth_r), 'color', [0.937 0 0.937], '-;Mid;' );
semilogx ( freqs_twth, (loud_twth_sr-loud_twth_r), 'color', [0 0.808 0.808], '-;Loud;' );
ylim ([-25 25]);
xlim ([200 16000]);
xticks(freqs_x);
xticklabels(freqs_x);
yticks(amps_yy);
yticklabels(amps_yy);
title("SP1 Sentibo G10C Right Ear\n Sentibo minus NL2", "fontsize", 18, "fontweight", "bold");
ylabel("dB Gain in HA-4 Coupler", "fontsize", 12, "fontweight", "bold");
xlabel("Frequency in Hz", "fontsize", 12, "fontweight", "bold");
legend("location", "northeast");
#}

# plot earpod responses Sentibo thin tube red
figure;
clf;
hold on;
red_o_peak = [41.9 41.7 41.3 40.6 39.5 37.4 34.8 33.0 32.0 31.5 31.1 30.9 31.1 31.1 30.8 30.9 30.7 30.3 30.1 30.0 30.1 30.4 30.6 31.0 31.3 31.3 31.3 31.4 31.6 31.7 31.5 31.4 31.6 32.1 32.9 33.4 33.3 32.8 32.7 32.8 32.1 31.5 32.1 32.8 33.5 34.0 34.2 34.2 34.0 33.9 33.9 34.6 35.7 36.9 37.9 38.6 38.9 39.2 39.7 40.4 41.1 41.6 42.1 42.7 43.2 43.5 43.9 44.4 44.6 44.6 44.5 44.4 44.2];
red_o_twth = [36.7 37.4 37.4 37.2 36.4 33.7 30.9 29.2 28.5 27.9 27.3 27.1 27.2 27.4 27.5 27.8 27.6 27.5 27.2 27.3 27.6 27.8 28.2 28.4 28.4 28.5 28.9 29.1 29.2 29.5 29.4 29.3 29.2 29.1 28.8 28.6 28.3 28.1 28.0 28.2 28.8 29.6 30.4 31.2 32.0 32.5 32.9 32.9 32.8 32.6 32.7 33.3 34.5 35.7 36.6 37.1 37.5 37.8 38.5 39.5 40.2 40.6 41.0 41.4 41.8 42.3 42.8 43.3 43.5 43.6 43.5 43.3 43.0];
red_o_valy = [34.2 34.8 35.2 35.1 34.2 32.1 29.6 27.8 26.9 26.4 26.0 26.0 26.1 26.3 26.6 26.9 26.8 26.5 26.4 26.4 26.6 26.9 27.3 27.6 27.7 27.9 28.1 28.3 28.5 28.7 28.8 28.6 28.5 28.3 28.0 27.7 27.4 27.1 27.1 27.5 28.2 29.1 30.0 30.8 31.5 32.1 32.4 32.4 32.3 32.2 32.5 33.2 34.3 35.4 36.2 36.7 37.1 37.5 38.3 39.1 39.8 40.2 40.7 41.1 41.5 42.0 42.5 42.9 43.2 43.2 43.1 43.0 42.9];
semilogx ( freqs_twth, red_o_peak, 'color', sentibo_color_red, '--' );
semilogx ( freqs_twth, red_o_twth, 'color', sentibo_color_red, '-' );
semilogx ( freqs_twth, red_o_valy, 'color', sentibo_color_red, '--' );
ylim ([-10 140]);
xlim ([200 16000]);
xticks(freqs_x);
xticklabels(freqs_x);
yticks(amps_y);
yticklabels(amps_y);
title("SPL Sentibo Earpod Occluded\n Red Color", "fontsize", 18, "fontweight", "bold");
ylabel("dB SPL on KEMAR (60318-4 OES)", "fontsize", 12, "fontweight", "bold");
xlabel("Frequency in Hz", "fontsize", 12, "fontweight", "bold");
#legend("location", "northeast");
