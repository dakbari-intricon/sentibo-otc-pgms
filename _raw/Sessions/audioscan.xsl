<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/session">
  <html><body>
    <p>
	  Session: <xsl:apply-templates select="@SessionID"/>, 
      Client: <xsl:apply-templates select="@ClientID"/>,
      Date: <xsl:apply-templates select="@date"/>,
      Time: <xsl:apply-templates select="@time"/>
    </p>

    <xsl:apply-templates select="equipment"/>

    <table border="1">
      <tr><th>Test</th><th>Side</th><th>Curve</th><th>Attributes</th><th>Data</th></tr>
      <xsl:apply-templates select="test"/>
		</table>

  </body></html>
</xsl:template>


<!-- Matches all test elements not explicitly covered elsewhere  -->
<xsl:template name="equip" match="/session/equipment" priority="1">
    <p>
	  Model: <xsl:apply-templates select="@model"/>, 
      Serial: <xsl:apply-templates select="@serial"/>,
      Software: <xsl:apply-templates select="@software"/>,
      DSL: <xsl:apply-templates select="@dsl"/>,
      XML-Format: <xsl:apply-templates select="@xmlformat"/>
    </p>
</xsl:template>


<!-- Matches all test elements not explicitly covered elsewhere  -->
<xsl:template match="/session/test" priority="1">
	<xsl:call-template name="show-test"/>
</xsl:template>

<!-- Matches just a particular test (for development purposes) 
<xsl:template match="/session/test[./@name = 'audiometry']" priority="2">
  <xsl:call-template name="show-test"/>
</xsl:template>
-->

<xsl:template name="show-test">
  <tr>
    <td> <xsl:value-of select="@name"/> </td><td><xsl:value-of select="@side"/></td><td></td>
    <td>
      <xsl:for-each select="./attribute::*[name(.) != 'name'][name(.)!='side']"> 
        <xsl:value-of select="name(.)"/> = "<xsl:value-of select="."/>"<xsl:text>, </xsl:text>
      </xsl:for-each>
    </td>
  </tr>
  <xsl:apply-templates/>
</xsl:template>

<!-- generic data element lister. Outputs a table of attribute names and values, followed by the data content, if any-->
<xsl:template match="data">
  <tr>
    <td></td><td></td><td><xsl:value-of select="@name"/></td>

    <td><xsl:for-each select="./attribute::*[name(.) != 'name'][name(.) != 'internal']"> 
    <xsl:value-of select="name(.)"/> = "<xsl:value-of select="."/>"<xsl:text>, </xsl:text>
    </xsl:for-each></td>
    <xsl:call-template name="datacells"> <xsl:with-param name="list"><xsl:value-of select="."/></xsl:with-param> </xsl:call-template>
  </tr>
</xsl:template>

<!-- recursively prints table cells for a list of numbers -->
<xsl:template name="datacells" > <xsl:param name="list"/>
<!--  <xsl:value-of select="$list"/> -->
  <td><xsl:value-of select="substring-before($list, ' ')"/>
    <xsl:if test="string-length(substring-before($list, ' '))=0">
      <!-- list is a single element -->
      <xsl:value-of select="$list"/>
    </xsl:if>
  </td>
  <xsl:if test="string-length(substring-after($list, ' '))>0">
    <!-- call self -->
	<xsl:call-template name="datacells"> <xsl:with-param name="list"><xsl:value-of select="substring-after($list, ' ')"/></xsl:with-param> </xsl:call-template>
  </xsl:if>
</xsl:template>

</xsl:stylesheet>
